package урок_2_java;

import org.junit.Assert;

public class Check {

    public static void check1(String value) {
        Assert.assertEquals("Неплохо! Идем ужинать в ресторан.", value);
    }

    public static void check2(String value) {
        Assert.assertEquals("Ваши сбережения в датской кроне: 1634.175084175084", value);
    }

    public static void check3(String value) {
        Assert.assertEquals("Этаж 5 квартира 20 — доставлено.", value);
    }

    public static void check4(String[] array) {
        String[] plans = {
                "Купить диван",
                "Отметить день рождения с друзьями",
                "Обустроить рабочее место",
                "Научиться вести бюджет",
                "Поехать в Китай по работе",
                "Сходить в караоке клуб",
                "Попросить на работе один удалённый день в неделю",
                "Заняться спортом",
                "Поехать в Сочи в отпуск",
                "Начать внедрять zero waste-подходы в повседневную жизнь",
                "Встречаться с друзьями не реже раза в неделю",
                "Перестать есть в кровати"
        };
        Assert.assertEquals(plans, array);
    }


}
