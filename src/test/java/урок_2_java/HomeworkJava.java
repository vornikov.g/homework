package урок_2_java;



import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;

import static урок_2_java.Check.*;

public class HomeworkJava {

    /*
   ***************************************************Задание 1*********************************************************
   Мы решили сегодня поужинать.
   Количество денег у нас аж 10000.0 единиц!
   Помогите нам сделать выбор.
   Вам необходимо:
   1. Дополнить условия таким образом, чтобы мы поужинали в ресторане.

   Проверка считается успешной, если в консоль выводится значение:
   Неплохо! Идем ужинать в ресторан.
    */
    @Test
    @DisplayName("Задание - где мы будем ужинать?")
    public void whereAreWeGoingToEat() {
        // TODO: Удалите символы комментария - /* и */

        /*

        double moneyBeforeSalary = 10000.0; // Количество денег
        String check;
        if (moneyBeforeSalary < 3000) {
            check = "А приготовим ка мы домашнюю еду!";
        } else if (moneyBeforeSalary < 5000) {
            check = "Окей, собирайтесь во Вкусно и точка!";
        } ... (...) {
            check = "Неплохо! Идем ужинать в ресторан.";
        } ... {
            check = "Класс! Закажем крабов и омаров!";
        }
        System.out.println(check);
        check1(check);

        */

    }


    /*
    ***************************************************Задание 2********************************************************
    Итак, в наш код прокралась ошибка!
    Вам предстоит её исправить:
    конвертация в доллары и евро работает, но проблема возникает,
    если попытаться конвертировать в новые валюты.
    Вам необходимо:
    1. Задать необходимый тип переменных
    2. Дополнить условия работы с валютами:
        Японская иена — JPY
        Датская крона — DKK
    3. Обработать условие для получения сообщения об ошибке

     Проверка считается успешной, если в консоль выводится значение:
     Ваши сбережения в датской кроне: 1634.175084175084
    */
    @Test
    @DisplayName("Задание - конвертер валют")
    public void currencyConverter() {
        // TODO: Удалите символы комментария - /* и */

        /*

        // TODO: Вместо ... задать необходимый тип переменных
        double rubles = 14560.5; //Количество рублей
        ... rateUSD = 78.5; //Курс доллара
        ... rateDKK = 8.91; //Курс датской кроны
        ... rateEUR = 85.5; //Курс евро
        ... rateJPY = 0.74; //Курс японской иены

        String currency = "DKK";
        String check;
        System.out.println("Вы конвертируете рубли в " + currency);
        // TODO: Вместо ... задать условия для работы с валютами JPY, DKK и обработать ошибку, если валюта не поддерживается
        if (currency.equals("USD")) {
            check = "Ваши сбережения в долларах: " + rubles / rateUSD;
        } else if (currency.equals("EUR")) {
            check = "Ваши сбережения в евро: " + rubles / rateEUR;
        } ... (...) {
            check = "Ваши сбережения в иенах: " + rubles / rateJPY;
        } ... (...) {
            check = "Ваши сбережения в датской кроне: " + rubles / rateDKK;
        } ... {
            check = "Валюта не поддерживается.";
        }
        System.out.println(check);
        check2(check);

        */
    }

     /*
    ***************************************************Задание 3********************************************************
    Сегодня необычный день — курьер привёз посылки для каждого жильца вашего дома!
    Нельзя пропустить ни одной квартиры — в каждую нужно что-то доставить.
    Чтобы не запутаться, курьер решил обходить все этажи и квартиры строго последовательно в порядке возрастания.
    Всего в доме 5 этажей. На каждом из них по 4 квартир — их число хранится в переменной flatsNumber.
    В переменной currentFlat сохраняется номер текущей квартиры.
    Используя вложенные циклы, напишите программу для учёта работы курьера, которая будет:
    1. Выводить в консоль номера этажей и квартир и сообщение, что посылка доставлена.

    Проверка считается успешной, если последним сообщением в консоль выводится значение:
    Этаж 5 квартира 20 — доставлено.
    */

    @Test
    @DisplayName("Задание - доставка")
    public void delivery() {
        // TODO: Удалите символы комментария - /* и */

        /*

        int floorsNumber = 5; // Количество этажей
        int flatsNumber = 4; // Количество квартир на этаже
        String result = ""; // Вспомогательная переменная для проверки итогового значения

        // TODO: В этом цикле необходимо пройтись по каждому этажу (с 1го по 5ый)
        for (int i = ...; i <= ...; i++) {
//            // TODO: В этом цикле необходимо пройтись по каждой квартире на этаже (с 1ой по 4ую)
            for (int j = ...; j <= ...; j++) {
                int currentFlat = ((i - 1) * flatsNumber + j); // Формула для расчета номера текущей квартиры
//                // TODO: Вместо ... задать текущий этаж и текущую квартиру
                result = "Этаж " + ... + " квартира " + ... + " — доставлено."
                System.out.println(result);
            }
        }
        check3(result);

         */

    }

     /*
    ***************************************************Задание 4********************************************************
    Ваш коллега запланировал на год дюжину классных целей и составил из них список в порядке приоритетности.
    Однако сразу после новогодних каникул планы изменились:
        1. Командировка в Китай отложена
        2. Диван нужен срочно — в гости приедут друзья
        3. Поездка в Японию вообще сорвалась — решено отдыхать в Сочи
        4. Музыкальный фестиваль отменили, поехать не получится =(

    Вам необходимо:
        1. Поменять местами поездку в Китай и покупку дивана
        2. Заменить отпуск в Японии отпуском в Сочи
        3. Заменить поездку на музыкальный фестиваль походом в караоке
        4. Вывести в консоль Изменения в планах
     */

    @Test
    @DisplayName("Задание - планы")
    public void plans() {
        // TODO: Удалите символы комментария - /* и */

        /*

        // Массив с планами коллеги
        String[] plans = {
                "Поехать в Китай по работе",
                "Отметить день рождения с друзьями",
                "Обустроить рабочее место",
                "Научиться вести бюджет",
                "Купить диван",
                "Поехать на музыкальный фестиваль",
                "Попросить на работе один удалённый день в неделю",
                "Заняться спортом",
                "Поехать в Японию в отпуск",
                "Начать внедрять zero waste-подходы в повседневную жизнь",
                "Встречаться с друзьями не реже раза в неделю",
                "Перестать есть в кровати"
        };

        // TODO: Поменяйте местами покупку дивана и поездку в Китай в три шага, используя swap
        String swap = ...; // Присвойте swap значение элемента, которое необходимо заменить
        ... = ...; // Присвойте значение одного элемента другому
        ... = swap; // В нужной ячейке должно оказаться значение элемента, выбранного на замену

        // TODO: Замените фразу "Поехать в Японию в отпуск" на "Поехать в Сочи в отпуск"
        ... = ...;

        // TODO: Замените фразу "Поехать на музыкальный фестиваль" на "Сходить в караоке клуб"
        ... = ...;

        // TODO: Вместо ... добавить измененные элементы массива
        System.out.println("Изменения в планах:");
        System.out.println("1. " + plans[0]);
        System.out.println("5. " + ...);
        System.out.println("6. " + ...);
        System.out.println("9. " + ...);

        check4(plans);

         */

    }




}
