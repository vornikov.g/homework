package урок_3_HTML;

import lib.NameOfElement;
import урок_3_HTML.ресурсы.GetElement;

public class ElementLocator extends GetElement {

// TODO: Раздел 1: Дополнить xpath фильтром

// TODO: 1. Локатор элемента «First Name», содержащий вместо ... фильтр по атрибуту placeholder

    @NameOfElement("Элемент 1")
    public String element1 = "//form//input[...]";

// TODO: 2. Локатор элемента «Subjects», содержащий вместо ... фильтр по тексту без использования метода contains

    @NameOfElement("Элемент 2")
    public String element2 = "//label[...]";

// TODO: 3. Локатор элемента div, дочерним элементом которого является узел label с контентом «Male»
//  содержащий вместо ... фильтр по тексту и метод contains

    @NameOfElement("Элемент 3")
    public String element3 = "//label[starts-with(@for,'gender') ...]/parent::div";

// TODO: Раздел 2: Расставить родственные связи в xpath

// TODO: 1. Локатор элемента div, дочерним элементом которого является узел label с контентом «Music»
//  вместо ... необходимо расставить родственные связи, используя комбинации символов «/» и «.»

    @NameOfElement("Элемент 4")
    public String element4 = "//form/div[@id='hobbiesWrapper']...label[text()='Music']...";

// TODO: 2. Локатор элемента «Sports», содержащий вместо первых двух ... родственные связи child и sibling,
//  вместо третьего ... фильтр по атрибуту for

    @NameOfElement("Элемент 5")
    public String element5 = "//div...input[@type='checkbox']...label[...]";

// TODO: 3.  Локатор элемента form, содержащий вместо ... связь «предок»

   @NameOfElement("Элемент 6")
    public String element6 = "//input[@id='gender-radio-2']/...form";

// TODO: Раздел 3: Составить простой CSS селектор

// TODO: 1. Вместо ... необходимо вставить простой селектор элемента «Выберите файл» по атрибуту id

    @NameOfElement("Элемент 7")
    public String element7 = "...";

// TODO: 2. Вместо ... необходимо вставить простой селектор элемента «Выберите файл» по атрибуту class

    @NameOfElement("Элемент 8")
    public String element8 = "...";

}
