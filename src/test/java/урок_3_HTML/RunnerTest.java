package урок_3_HTML;

import com.codeborne.selenide.Configuration;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm",
        },
        features = {"src/test/java/урок_3_HTML"},
        tags = {"ЗДЕСЬ ДОЛЖЕН БЫТЬ ТЕГ ТЕСТА"},
        stepNotifications = true
)

public class RunnerTest {

    @BeforeClass
    static public void setup() {
        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
    }
}

