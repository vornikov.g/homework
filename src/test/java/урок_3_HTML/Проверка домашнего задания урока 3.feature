# language: ru
@HomeWorkLesson3
Функционал: Проверка домашнего задания урока № 3

  @HomeWorkPart1HTML
  Сценарий: Проверка файла Index.html
    Когда Пользователь открывает файл Index.html в браузере
    То Пользователь проверяет страницу Index.html на правильность выполнения

  @HomeWorkPart2XPath
  Структура сценария: Проверка xpath элемента "<nameOfElement>"
    Когда Пользователь открывает страницу Practice Form
    То Пользователь проверяет наличие элемента "<nameOfElement>" на странице по "xpath"
    И Пользователь проверяет количество элементов "<nameOfElement>" на странице по "xpath"
    И Пользователь проверяет локатор элемента "<nameOfElement>" на соответствие ожидаемому
    Примеры:
      | nameOfElement |
      | Элемент 1     |
      | Элемент 2     |
      | Элемент 3     |
      | Элемент 4     |
      | Элемент 5     |
      | Элемент 6     |

  @HomeWorkPart2CSS
  Структура сценария: Проверка CSS элемента "<nameOfElement>"
    Когда Пользователь открывает страницу Practice Form
    То Пользователь проверяет наличие элемента "<nameOfElement>" на странице по "CSS"
    И Пользователь проверяет количество элементов "<nameOfElement>" на странице по "CSS"
    И Пользователь проверяет селектор элемента "<nameOfElement>" на соответствие ожидаемому
    Примеры:
      | nameOfElement |
      | Элемент 7     |
      | Элемент 8     |
