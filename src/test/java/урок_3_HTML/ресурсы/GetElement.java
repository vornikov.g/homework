package урок_3_HTML.ресурсы;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lib.NameOfElement;
import org.openqa.selenium.By;

import java.lang.reflect.Field;

import static com.codeborne.selenide.Selenide.*;

public class GetElement {

    public SelenideElement element(String value) {
        return $x(value);
    }

    public ElementsCollection elementsCollection(String value) {
        return $$x(value);
    }

    public SelenideElement elementByCss(String value) {
        return $(By.cssSelector(value));
    }

    public ElementsCollection elementsCollectionByCss(String value) {
        return $$(By.cssSelector(value));
    }

    public String get(String cucumberElementName) {
        return (String) getElements(cucumberElementName);
    }

    public Object getElements(String cucumberElementName) {
        try {
            for (Field field : this.getClass().getDeclaredFields())
                if (searchField(cucumberElementName, field))
                    return field.get(this);
        } catch (IllegalAccessException e) {
            System.out.println("ОШИБКА: Элемент с именем '" + cucumberElementName + "' страницы " + this.getClass().getName() + " не является общедоступным");
        }
        throw new IllegalArgumentException("ОШИБКА: элемент с именем '" + cucumberElementName + "' отсутствует в классе " + this.getClass().getName());
    }

    public boolean searchField(String cucumberElementName, Field field) {
        if (field.isAnnotationPresent(NameOfElement.class)) {
            NameOfElement nameOfElementAnnotation = field.getAnnotation(NameOfElement.class);
            return nameOfElementAnnotation.value().equals(cucumberElementName);
        }
        return false;
    }

    public static String expected1 = "//form//input[@placeholder='First Name']";
    public static String expected2 = "//label[text()='Subjects']";
    public static String expected3 = "//label[starts-with(@for,'gender') and contains(text(),'Male')]/parent::div";
    public static String expected4 = "//form/div[@id='hobbiesWrapper']//label[text()='Music']/..";
    public static String expected5 = "//div/child::input[@type='checkbox']/following-sibling::label[@for='hobbies-checkbox-1']";
    public static String expected6 = "//input[@id='gender-radio-2']/ancestor::form";
    public static String expected7 = "#uploadPicture";
    public static String expected8 = ".form-control-file";
    public static String expected9 = "<!DOCTYPEhtml><htmllang=\"ru\"><head><metacharset=\"UTF-8\"><title>ОЦРВ</title></head><body><h2>Компетенции</h2><ul><li>ИТ-консалтинг</li><li>Комплекснаяавтоматизация</li><li>Поддержкаисопровождение</li></ul></body></html>";

}
