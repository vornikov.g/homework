package урок_3_HTML.ресурсы;

import com.codeborne.selenide.Condition;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.То;
import io.qameta.allure.Allure;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import урок_3_HTML.ElementLocator;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static урок_3_HTML.ресурсы.GetElement.*;

public class Steps {
    ElementLocator elementLocator = new ElementLocator();
    GetElement thisPage = new GetElement();

    @Когда("^Пользователь открывает страницу Practice Form$")
    public void пользовательОткрываетСтраницуPracticeForm() {
        open("https://demoqa.com/automation-practice-form");
        $x("//img[@src='/images/Toolsqa.jpg']").shouldBe(Condition.appear, Duration.ofSeconds(15));
    }

    @И("^Пользователь проверяет наличие элемента \"([^\"]*)\" на странице по \"([^\"]*)\"$")
    public void пользовательПроверяетНаличиеЭлементаНаСтранице(String nameOfElement, String locatorType) throws Exception {
        switch (locatorType){
            case "xpath":
                if(!thisPage.element(elementLocator.get(nameOfElement)).exists()){
                    throw new Exception("Элемент отсутствует на странице. Локатор элемента '" + nameOfElement + "' найден некорректно.");
                }
                break;
            case "CSS":
                if(!thisPage.elementByCss(elementLocator.get(nameOfElement)).exists()){
                    throw new Exception("Элемент отсутствует на странице. Селектор элемента '" + nameOfElement + "' найден некорректно.");
                }
                break;
        }
    }

    @И("^Пользователь проверяет локатор элемента \"([^\"]*)\" на соответствие ожидаемому$")
    @Когда("^Пользователь проверяет селектор элемента \"([^\"]*)\" на соответствие ожидаемому$")
    public void пользовательПроверяетЛокаторЭлементаНаСоответствиеОжидаемому(String nameOfElement) {
        String actualValue = elementLocator.get(nameOfElement).replaceAll(" ", "");
        String messageXpath = "Проверьте, что локатор элемента '" + nameOfElement + "' найден в соответствии с заданием." ;
        String messageCss = "Проверьте, что селектор элемента '" + nameOfElement + "' найден в соответствии с заданием." ;
        switch (nameOfElement) {
            case "Элемент 1":
                if (!expected1.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageXpath);
                }
                break;
            case "Элемент 2":
                if (!expected2.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageXpath);
                }
                break;
            case "Элемент 3":
                if (!expected3.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageXpath);
                }
                break;
            case "Элемент 4":
                if (!expected4.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageXpath);
                }
                break;
            case "Элемент 5":
                if (!expected5.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageXpath);
                }
                break;
            case "Элемент 6":
                if (!expected6.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageXpath);
                }
                break;
            case "Элемент 7":
                if (!expected7.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageCss);
                }
                break;
            case "Элемент 8":
                if (!expected8.replaceAll(" ", "").equals(actualValue)) {
                    Assert.fail(messageCss);
                }
                break;
        }
    }

    @И("^Пользователь проверяет количество элементов \"([^\"]*)\" на странице по \"([^\"]*)\"$")
    public void пользовательПроверяетКоличествоЭлементовНаСтранице(String nameOfElement, String locatorType) throws Exception {
        int size = 0;
        switch (locatorType){
            case "xpath":
                size = thisPage.elementsCollection(elementLocator.get(nameOfElement)).size();
                break;
            case "CSS":
                size = thisPage.elementsCollectionByCss(elementLocator.get(nameOfElement)).size();
                break;
        }
        if (size != 1) {
            throw new Exception("Локатор элемента '" + nameOfElement + "' находит " + size + " элементов, а должен находить один.");
        }
    }

    @Когда("Пользователь открывает файл Index.html в браузере")
    public void пользовательОткрываетФайлIndexHtmlВБраузере() {
        File file = new File("src/test/java/урок_3_HTML/Index.html");
        String path = file.getAbsolutePath();
        open("file://" + path);
    }

    @То("Пользователь проверяет страницу Index.html на правильность выполнения")
    public void пользовательПроверяетКодВебСтраницыНаСоответствиеОжидаемому() throws Exception {
        String actual = new String(Files.readAllBytes(
                Paths.get("src/test/java/урок_3_HTML/Index.html")),
                StandardCharsets.UTF_8)
                .replaceAll(" ", "")
                .replaceAll("\n", "")
                .replaceAll("\r", "");
        Assert.assertTrue("Html-код составлен некорректно.", expected9.equals(actual));
    }

    @After
    public void afterTest(Scenario scenario) {
        WebDriver driver = getWebDriver();
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }
        closeWebDriver();
    }

}
