package урок_4_selenium;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckResults {

//        # Метод проверяет, присутствует ли alert на странице
    public static boolean isAlertPresent(ChromeDriver driver)
    {
        try
        {
            driver.switchTo().alert();
            return true;
        }
        catch (NoAlertPresentException Ex)
        {
            return false;
        }
    }

    public static void checkTest1(WebElement firstName, WebElement lastName, WebElement email){
        Assert.assertEquals("Leo", firstName.getAttribute("value"));
        Assert.assertEquals("Messi", lastName.getAttribute("value"));
        Assert.assertEquals("LeoMessi@example.com", email.getAttribute("value"));
    }

    public static void checkTest2(ChromeDriver driver){
        WebElement genderRadioButtonInput = driver.findElement(By.xpath("//input[@name='gender' and @value ='Male']"));
        Assert.assertTrue(genderRadioButtonInput.isSelected());
    }

    public static void checkTest3(WebElement mobileNumber){
        Assert.assertEquals("8123456789", mobileNumber.getAttribute("value"));
    }

    public static void checkTest4(WebElement dateOfBirth){
        Assert.assertEquals("15 Jan 1980", dateOfBirth.getAttribute("value"));
    }

    public static void checkTest5(ChromeDriver driver){
        WebElement hobbiesCheckboxInput = driver.findElement(By.xpath("//input[@id='hobbies-checkbox-1']"));
        Assert.assertTrue(hobbiesCheckboxInput.isSelected());
    }

    public static void checkTest6(WebElement uploadPicture){
        Assert.assertEquals("C:\\fakepath\\photo.jpg", uploadPicture.getAttribute("value"));
    }

    public static void checkTest7(WebElement currentAddress){
        Assert.assertEquals("Spain/Barcelona", currentAddress.getAttribute("value"));
    }

    public static void checkTest8(ChromeDriver driver){
        WebElement stateValue = driver.findElement(By.xpath("//div[@id='state']//div[@class=' css-1uccc91-singleValue']"));
        WebElement cityValue = driver.findElement(By.xpath("//div[@id='city']//div[@class=' css-1uccc91-singleValue']"));
        Assert.assertEquals("Haryana", stateValue.getText());
        Assert.assertEquals("Panipat", cityValue.getText());
    }

    public static void checkTest9(WebElement submitButton){
        Assert.assertEquals("Submit", submitButton.getText());
    }

    public static void checkTestWaitsAlert(ChromeDriver driver){
        Assert.assertFalse("Alert присутствует на странице, хотя должен отсутствовать", isAlertPresent(driver));
    }

    public static void checkTestWaits3(WebElement btnEnabled){
        Assert.assertEquals("Enabled", btnEnabled.getText());
    }

    public static void checkTestWaits4(WebElement btnButton){
        Assert.assertTrue("Элемент btnButton недоступен", btnButton.isEnabled());
    }
}
