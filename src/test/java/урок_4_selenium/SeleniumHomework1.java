package урок_4_selenium;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Step;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.time.Duration;

import static урок_4_selenium.CheckResults.*;


public class SeleniumHomework1 {

    private ChromeDriver driver;

    private WebElement lastName;
    private WebElement email;



    @Before
    public void setUp(){
//        # WebDriverManager автоматически проверяет версию браузера, если нужно, то скачивает актуальный chromeDriver
        WebDriverManager.chromedriver().setup();

//        # Если нужно разворачивать окно браузера на весь экран, то прописываем это в настройках и передаем параметр
//        'co' драйверу
        ChromeOptions co = new ChromeOptions();
        co.addArguments("--window-size=1920,1080");
        co.addArguments("--remote-allow-origins=*");

//        # Инициализируем драйвер браузера. После этой команды вы должны увидеть новое открытое окно браузера
         driver = new ChromeDriver(co);

//        # Метод get сообщает браузеру, что нужно открыть сайт по указанной ссылке
        driver.get("https://demoqa.com/automation-practice-form");

//        # Установим неявное ожидание элементов
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

    }

    @After
    public void tearDown() {
        driver.quit();
    }


//  #TODO: Задание: Заполнить форму регистрации

    @Test
    @Step("Заполнить поля: First name, Last name, Email.")
    public void test1(){
//      #TODO: 1) Заполнить поля First name значением Leo, Last name значением Messi, Email значением LeoMessi@example.com. Найти элемент с помощью id
//      # Присваиваем значение элементу
        WebElement firstName = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
//        # Метод очистки текста в поле
        firstName.clear();
//        # Метод ввода текста в поле
        firstName.sendKeys("ЗДЕСЬ ДОЛЖНО БЫТЬ ПЕРЕДАВАЕМОЕ ЗНАЧЕНИЕ");
//        # ЗАПОЛНИТЕ ОСТАВШИЕСЯ ПОЛЯ ПО АНАЛОГИИ С firstName
//        lastName
//        email
//        # Проверка результатов теста
        checkTest1(firstName, lastName, email);
    }

    @Test
    @Step("Выбрать: Gender Male.")
    public void test2(){
//      #TODO: 2) Выбрать Gender Male. Найти нужный radiobutton элемент с помощью xpath (нужно использовать ancestor)
//      # Присваиваем значение элементу
        WebElement genderRadioButton = driver.findElement(By.xpath("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ XPATH"));
//      # Выбираем элемент
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//        # Проверка результатов теста
        checkTest2(driver);
    }

    @Test
    @Step("Заполнить поле: Mobile Number.")
    public void test3(){
//      #TODO: 3) Заполнить поле Mobile Number значением 8123456789. Найти элемент с помощью id
//      # Присваиваем значение элементу
        WebElement mobileNumber = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//        # Проверка результатов теста
        checkTest3(mobileNumber);
    }

    @Test
    @Step("Заполнить поле: Date of Birth.")
    public void test4() {
//      #TODO: 4) Заполнить поле Date of Birth. Найти элемент с помощью id
        WebElement dateOfBirth = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
        dateOfBirth.click();
//      #TODO: Выбрать месяц January. Найти нужный элемент с помощью xpath
        WebElement selectMonth = driver.findElement(By.xpath("//select[@class='react-datepicker__month-select']/ДОПИШИТЕ ЗДЕСЬ ВАШ XPATH"));
        selectMonth.click();

//      #TODO: Выбрать год 1980. Найти нужный элемент с помощью xpath
        WebElement selectYear = driver.findElement(By.xpath("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ XPATH"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД

//      #TODO: Выбрать число 15. Найти нужный элемент с помощью xpath
        WebElement selectDay = driver.findElement(By.xpath("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ XPATH"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД

//        # Проверка результатов теста
        checkTest4(dateOfBirth);
    }

    @Test
    @Step("В графе Hobbies - отметить чек боксом Sports.")
    public void test5() {

/*      #TODO: 5) В графе Hobbies - отметить чек боксом Sports. Найти нужный элемент с помощью xpath
            (нужно использовать ancestor)
 */
        WebElement hobbiesCheckbox = driver.findElement(By.xpath("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ XPATH"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//        # Проверка результатов теста
        checkTest5(driver);
    }

    @Test
    @Step("В графе Picture - загрузить фото.")
    public void test6() {
/*      #TODO: 6) В графе Picture - загрузить фото. Найти нужный элемент с помощью xpath (загружать фото можно
            в элемент для загрузки фото, с тэгом input)
 */
        File file = new File("res/selenium_images/photo.jpg");
        WebElement uploadPicture = driver.findElement(By.xpath("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ XPATH"));
        uploadPicture.sendKeys(file.getAbsolutePath());
//        # Проверка результатов теста
        checkTest6(uploadPicture);
    }

    @Test
    @Step("Заполнить поле Current Address.")
    public void test7() {
//      #TODO: 7) Заполнить поле Current Address значением Spain/Barcelona.
        WebElement currentAddress = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД
//        # Проверка результатов теста
        checkTest7(currentAddress);
    }

    @Test
    @Step("Заполнить поле Select State and Select City.")
    public void test8() {
//      #TODO: 8) Заполнить поле Select State and Select City
//      #TODO: Кликнуть на поле state
        WebElement state = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//      #TODO: Выбрать state Haryana. Найти нужный элемент с помощью xpath
        WebElement selectState = driver.findElement(By.xpath("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ XPATH"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//      #TODO: Кликнуть на поле city
        WebElement city = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//      #TODO: Выбрать city Panipat
        WebElement selectCity = driver.findElement(By.xpath("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ XPATH"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//        # Проверка результатов теста
        checkTest8(driver);

    }

    @Test
    @Step("Нажать на кнопку Submit.")
    public void test9() {
//      #TODO: 9) Нажать на кнопку Submit.
        WebElement submitButton = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
//        ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//        # Проверка результатов теста
        checkTest9(submitButton);
    }


}
