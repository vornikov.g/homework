package урок_4_selenium;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Step;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import static java.lang.Thread.sleep;
import static урок_4_selenium.CheckResults.*;


public class SeleniumHomework2Waits {
    private ChromeDriver driver;
    private WebElement btnEnabled;
    private Alert alert;


//        # Данный модуль выполняется до запуска теста, как предусловие
    @Before
    public void setUp(){
//        # WebDriverManager автоматически проверяет версию браузера, если нужно, то скачивает актуальный chromeDriver
        WebDriverManager.chromedriver().setup();

//        # Если нужно разворачивать окно браузера на весь экран, то прописываем это в настройках и передаем параметр
//        'co' драйверу
        ChromeOptions co = new ChromeOptions();
        co.addArguments("--window-size=1920,1080");
        co.addArguments("--remote-allow-origins=*");

//        # Инициализируем драйвер браузера. После этой команды вы должны увидеть новое открытое окно браузера
        driver = new ChromeDriver(co);

//        # Метод get сообщает браузеру, что нужно открыть сайт по указанной ссылке
        driver.get("https://chercher.tech/practice/explicit-wait-sample-selenium-webdriver");

    }

//        # Данный модуль выполняется после запуска теста, как постусловие
    @After
    public void tearDown() {
        driver.quit();
    }

/*  #TODO: Задание №1. Кликнуть на зеленую кнопку 'Click me, to Open an alert after 5 seconds', в появившемся alert окне
        нажать 'OK'. В качесте ожидания использовать задержку времени.
*/
    @Test
    @Step("Кликнуть на зеленую кнопку 'Click me, to Open an alert after 5 seconds', в появившемся alert окне нажать" +
            " 'OK'. В качестве ожидания использовать задержку времени.")
    public void test1() throws InterruptedException {
//      #TODO: Кликнуть на зеленую кнопку 'Click me, to Open an alert after 5 seconds
        WebElement greenBtnAlert = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
        greenBtnAlert.click();
//      #TODO: Дождаться появления alert
//      ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД (В качесте ожидания использовать задержку времени (метод sleep))
//      #TODO: Перейти в alert и нажать OK
        Alert alert = driver.switchTo().alert();
        alert.accept();
//        # Проверка результатов теста
        checkTestWaitsAlert(driver);
    }

/*  #TODO: Задание №2. Кликнуть на зеленую кнопку 'Click me, to Open an alert after 5 seconds', в появившемся alert окне
        нажать 'OK'. В качесте ожидания использовать явные ожидания (explicit waits).
 */
    @Test
    @Step("Кликнуть на зеленую кнопку 'Click me, to Open an alert after 5 seconds', в появившемся alert окне нажать" +
            " 'OK'. В качестве ожидания использовать явные ожидания (explicity waits).")
    public void test2() {
//      #TODO: Кликнуть на зеленую кнопку 'Click me, to Open an alert after 5 seconds'
        WebElement greenBtnAlert = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
        greenBtnAlert.click();
//      #TODO: Дождаться появления alert
//      Alert alert= ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД;
//      #TODO: Перейти в alert и нажать OK
        driver.switchTo().alert();
        alert.accept();
//        # Проверка результатов теста
        checkTestWaitsAlert(driver);
    }

/*  #TODO: Задание №3. Кликнуть на синюю кнопку 'Display button after 10 seconds', дождаться отображения кнопки
        'Enabled' (с помощью неявных ожиданий) и кликнуть на нее
 */
    @Test
    @Step("Кликнуть на синюю кнопку 'Display button after 10 seconds', дождаться отображения кнопки 'Enabled'" +
            " (с помощью неявных ожиданий) и кликнуть на нее")
    public void test3() {
//      #TODO: Кликнуть на синюю кнопку 'Display button after 10 seconds'
        WebElement blueBtnDisplay = driver.findElement(By.id("display-other-button"));
        blueBtnDisplay.click();
//      #TODO: Дождаться появления кнопки 'Enabled' (с помощью неявных ожиданий)
//      ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД
//      #TODO: Кликнуть по кнопке
        WebElement btnEnabled = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
        btnEnabled.click();
//        # Проверка результатов теста
        checkTestWaits3(btnEnabled);
    }

/*  #TODO: Задание №4. Кликнуть на синюю кнопку 'Enable button after 10 seconds', дождаться, когда кнопка 'Button'
        станет доступной (с помощью явных ожиданий) и кликнуть на нее
 */
    @Test
    @Step("Кликнуть на синюю кнопку 'Enable button after 10 seconds', дождаться, когда кнопка 'Button' станет доступной" +
            " (с помощью явных ожиданий) и кликнуть на нее")
    public void test4() {
//      #TODO: Кликнуть на синюю кнопку 'Enable button after 10 seconds'
        WebElement blueBtnEnable = driver.findElement(By.id("enable-button"));
        blueBtnEnable.click();
//      #TODO: Дождаться появления кнопки 'Button' (с помощью явных ожиданий) и кликнуть по кнопке
        WebElement btnButton = driver.findElement(By.id("ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ ID"));
//      ЗДЕСЬ ДОЛЖЕН БЫТЬ ВАШ КОД
//        # Проверка результатов теста
        checkTestWaits4(btnButton);
    }
}

