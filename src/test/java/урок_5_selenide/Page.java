package урок_5_selenide;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.*;

public class Page {

    public SelenideElement btnCustomerLogin = $x("//button[@ng-click='customer()']");

    public SelenideElement selectYourName = $(byId("userSelect"));

    public SelenideElement btnLogin = $x("//button[text()='Login']");

    public SelenideElement welcome = $x("//*[@class='fontBig ng-binding']");

    public SelenideElement accountSelect = $(byId("accountSelect"));

    public SelenideElement info = $x("//div[@ng-hide='noAccount'][1]");

    public SelenideElement tabDeposit =  $x("//button[@ng-click='deposit()']");

    public SelenideElement amount =  $x("//input[@placeholder='amount']");

    public SelenideElement btnDeposit = $x("//button[text()='Deposit']");

    public SelenideElement getElementByText(String value) {
        return $x("//*[text()='" + value + "']");
    }


}
