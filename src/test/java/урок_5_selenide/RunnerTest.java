package урок_5_selenide;

import com.codeborne.selenide.Configuration;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm"
        },
        features = {"src/test/java/урок_5_selenide"},
        tags = {"@XYZ_Bank"},
        glue = {"урок_5_selenide"},
        stepNotifications = true
)

public class RunnerTest {


    @BeforeClass
    static public void setup() {
        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
    }
}

