package урок_5_selenide;


import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.То;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;



import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverConditions.url;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Steps {

    private final Page page = new Page();

    @Когда("^Пользователь открывает страницу 'XYZ Bank'$")
    public void пользовательОткрываетСтраницуXYZBank() {
        /* TODO: Необходимо использовать метод открытия экземпляра браузера и перехода по ссылке:
            https://www.globalsqa.com/angularJs-protractor/BankingProject/#/login
         */

    }

    @То("Система открывает начальную страницу 'XYZ Bank'")
    public void системаОткрываетНачальнуюСтраницуXYZBank() {
        webdriver().shouldHave(url("https://www.globalsqa.com/angularJs-protractor/BankingProject/#/login"));
    }

    @Когда("Пользователь нажимает кнопку 'Customer Login'")
    public void пользовательНажимаетКнопкуCustomerLogin() {
        // TODO: Необходимо нажать кнопку  btnCustomerLogin

    }

    @То("Система отображает выпадающий список 'Your Name'")
    public void системаОтображаетВыпадающийСписокYourName() {
        // TODO: Необходимо проверить, что элемент selectYourName видимый

    }

    @Когда("Пользователь в выпадающем списке 'Your Name' выбирает значение {string}")
    public void пользовательВВыпадающемСпискеYourNameВыбираетЗначение(String value) {
        page.selectYourName.selectOption(value);
    }

    @Когда("Пользователь нажимает кнопку 'Login'")
    public void пользовательНажимаетКнопкуLogin() {
        // TODO: Необходимо нажать кнопку  btnLogin

    }

    @То("Система открывает страницу приветствия {string}")
    public void системаОткрываетСтраницу(String value) {
        page.welcome.shouldHave(text(value));
    }

    @Когда("Пользователь в выпадающем списке 'Выбор счета' выбирает значение {string}")
    public void пользовательВВыпадающемСпискеВыборСчетаВыбираетЗначение(String value) {
        // TODO: Необходимо в выпадающем списке элемента accountSelect выбрать значение value

    }

    @И("Система отображает информацию {string}")
    public void системаОтображаетИнформацию(String value) {
        page.info.shouldHave(text(value));
    }

    @И("Пользователь нажимает на вкладку 'Deposit'")
    public void пользовательНажимаетНаВкладкуDeposit() {
        // TODO: Необходимо нажать кнопку  tabDeposit

    }

    @То("Система отображает поле ввода 'Amount to be Deposited'")
    public void системаОтображаетПолеВводаAmountToBeDeposited() {
        // TODO: Необходимо проверить, что элемент amount видимый

    }

    @Когда("Пользователь вводит в поле ввода 'Amount to be Deposited' значение {string}")
    public void пользовательВводитВПолеВводаAmountToBeDepositedЗначение(String value) {
        // TODO: Необходимо ввести в элемент amount значение value

    }

    @Когда("Пользователь нажимает кнопку 'Deposit'")
    public void пользовательНажимаетКнопкуDeposit() {
        page.btnDeposit.click();
    }

    @То("Система отображает значение {string}")
    public void системаОтображаетЗначение(String value) {
        page.getElementByText(value).shouldBe(visible);
    }

    @After
    public void afterTest(Scenario scenario) {
        WebDriver driver = getWebDriver();
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }
        closeWebDriver();
    }

}
